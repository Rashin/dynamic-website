// const { UUID } = require("bson");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const user = new Schema({
  
  firstname: {
    type: String,
    required: false
  },
  lastname: {
    type: String,
    required: false
  },
  email: {
    type: String,
    require: true,
    match: /.+\@.+\..+/,
    unique: true
  },
  username: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  description: {
    type: String,
    require: false
  },
  role: {
    type: String,
    require: true
  },
  status: {
    type: Number,
    require: false
  },
  lastlogin: {
    type: Number,
    require: true
  },
  basket: {
    productid: {
      type: [String],
      require: false,
    }
  }, 
  createddate: {
    type: Number,
    require: true
  },
  updateddate: {
    type: Number,
    require: false
  },
  googleid:{
    type:String,
    require:false
  },
  facebookid:{
    type:String,
    require:false
  },
  appleid:{
    type:String,
    require:false
  },
  isActive:{
    type:Number,
    require:true
  }
});

module.exports = mongoose.model("user", user);