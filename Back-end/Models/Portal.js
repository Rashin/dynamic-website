const mongoose = require('mongoose')
const schema = mongoose.schema

const Portal = new schema({
    header:{
        type:String,
        require:true
    },
    footer:{
        type:String,
        require:true
    },
    maincontent:{
        type:String,
        require:false
    },
    logo:{
        type:String,
        require:true
    },
    webinfo:{
        keyword:{
            type:String,
            require:false
        },
        description:{
            type:String,
            require:false
        }
    },
    theme:{
        layout:{
            type:String,
            require:false
        },
        colour:{
            type:String,
            require:false
        },
        manu:{
            title:{
                type:String,
                require:false
            },
            linke:{
                type:String,
                require:false
            },
            target:{
                type:String,
                require:false
            },
            children:{
                title:Boolean,
                require:false
            }
        }
    }
})

module.exports = mongoose.model('Portal',portal)