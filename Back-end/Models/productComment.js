const { UUID } = require("bson");
const mongoose = require("mongoose");
const schema = mongoose.schema;

const productComment = new schema({
    userid:{
       type:{UUID},
       require:true
    },
    productid:{
        type:{ÜÜID},
        require:true
    },
    commenttext:{
        type:String,
        require:false
    },
    likedislike:{
        type:Number,
        require:false
    },
    favourite:{
        type:Number,
        require:false
    }
});

module.exports = mongoose.model("productComment",productComment)