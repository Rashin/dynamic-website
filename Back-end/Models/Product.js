const { UUID } = require("bson");
const mongoose = require("mongoose");
const schema = mongoose.schema;

const Product = new schema({
  title: {
    type: String,
    require: true,
  },
  description: {
    type: String,
    require: false,
  },
  isActive: {
    typeof: Number,
    require: true,
  },
  status: {
    type: String,
    require: false,
  },
  image: {
    type: String,
    require: flase,
  },
  price: {
    type: Number,
    require: false,
  },
  count: {
    type: Number,
    require: false,
  },
  groupId: {
    type: { UUID },
    require: true,
  },
  brandid: {
    type: { UUID },
    require: true,
  },
  createddate: {
    type: Date,
    require: true,
  },
  updateddate: {
    type: Date,
    require: true,
  },
});

module.exports = mongoose.model("product", Product);
