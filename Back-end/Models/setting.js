const mongoose = require('mongoose')
const schema = mongoose.schema


const Setting = new schema({
    language:{
        type:String,
        require:false
    }
})

module.exports = mongoose.model('setting',Setting)