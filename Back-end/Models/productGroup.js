const {UUID} = require("bson")
const mongoose = require('mongoose')
const schema =mongoose.schema

const productgroup = new schema({
    title:{
        type:String,
        require:true
    },description:{
        type:String,
        require:false
    },
    createddate:{
        type:Date,
        require:true
    },
    updateddate:{
        type:Date,
        require:true
    }
});

module.exports = mongoose.model("productgroup",productgroup)