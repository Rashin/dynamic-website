const express = require("express");
const cors = require('cors');
const app = express();
const bodyParser = require("body-parser");
const userRouts = require("./Controllers/User/UserRout");
const userController = require('./Controllers/User/CUser');
const dbConnection = require('./Controllers/DBConnection')

const port = "4000";
//app.use(bodyParser.json());
 
app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

dbConnection.getConnected();

app.use('/user',userRouts);

// app.post('/user/login',function(req,res){
//     userController.Login(req,res);
// }) 

// app.post("/user", function (req, res) {
//    // res.send(req.body.firstname)
//   userController.CreateUser(req,res);
// });

app.listen(port);
