const express = require('express'); 
const router = express.Router(); 
const cUser = require('./CUser')

router.post('/signup', cUser.createUser)

router.post('/login', cUser.login)

router.put('/updateuser/:id', cUser.updateUser)

router.put('/deactive/:id', cUser.deactiveUser)

router.get('/getusers', cUser.getAllUsers)

router.get('/getuser/:id', cUser.getUserById)


module.exports = router