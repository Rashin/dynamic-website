//const { ObjectID } = require("bson");

const { ObjectId } = require("mongodb");
const User = require("../../Models/user");
const DBConnection = require("../DBConnection");

const createUser = async (req, res, next) => {
  try {
    const newuser = new User({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      description: req.body.description,
      role: 1,
      status: "",
      lastlogin: Date.now(),
      basket: req.body.basket,
      createddate: Date.now(),
      updateddate: Date.now(),
      googleid: req.body.googleid,
      facebookid: req.body.facebookid,
      appleid: req.body.appleid,
      isActive: 1,
    });
    await newuser.save(function (err, result) {
      if (err) res.json({message : "error is : " + err});
      if (result) res.json({result : result});
    });
    next;
  } catch (err) {
    throw err;
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    await User.find({ email: email, password: password }, function (err, obj) {
      if (err) res.json({ message: "Login faild." });
      else {
        res.json({ message: "user " + obj[0].username + " Welcome!" });
      }
    });
  } catch (err) {
    throw err;
  }
};

const getAllUsers = async (req, res, next) => {
  try {
    const Users = await User.find();
    res.json(Users);
  } catch (error) {
    throw error;
  }
};

const getUserById = async (req, res, next) => {
  try {

    const Users = await User.findOne({_id : req.params.id},function(error,result){
      if(error!== null){res.json({error : error})}
      else{res.json({result : result})}
    });
    res.json(Users);
  } catch (error) {
    throw error;
  }
};

const deactiveUser = async (req, res, next) => {
  try {
    const id = req.params.id;
    await User.updateOne(
      { _id: ObjectId(id) },
      { isActive: 0 },
      function (err, obj) {
        if (err !== null) {
          res.json({ message: err });
        } else {
          res.json({ message: "User Updated" });
        }
      }
    );
  } catch (error) {
    throw error;
  }
};

const updateUser = async (req, res, next) => {
  const id = req.params.id;
  try {
    await User.findOneAndUpdate(
      { _id: ObjectId(id) },
      {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
        description: req.body.description,
        updateddate: Date.now(),
      },
      { runValidators: true, useFindAndModify: false },
      function (error, result) {
        if (error !== null) {
          res.json({ message: error });
        } else {
          res.send(result);
        }
      }
    );
    res.json({ message: updates });
  } catch (err) {
    throw err;
  }
};

exports.createUser = createUser;
exports.updateUser = updateUser;
exports.login = login;
exports.getAllUsers = getAllUsers;
exports.deactiveUser = deactiveUser;
exports.getUserById = getUserById;
