const mongoose = require("mongoose");
const url = "mongodb://127.0.0.1:27017/DynamicDB";

const getConnected = async() => {
    try {
        return await mongoose.connect(url, { useUnifiedTopology: true,
             useNewUrlParser: true, useCreateIndex: true });
    }
    catch(err) {
        throw err;
    }   
}

const disconnect = async()=>{
    try{
        await mongoose.close;
    }
    catch(err){throw err}
}

exports.getConnected = getConnected;
exports.dicsonnect = disconnect;