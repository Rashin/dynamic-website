import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./index.css";
import App from "./App";
import User from './Components/Admin/Users'
import AddUser from './Components/Admin/AddUser'
import Products from './Components/Products/Products'
import Basket from './Components/Basket/Basket'

import reportWebVitals from "./reportWebVitals";

ReactDOM.render(
  <Router>
    <Routes>
      <Route path="/" element={<App />}> 
      </Route>
      <Route path="/users" element={<User />}></Route>
      <Route path="/users/add" element={<AddUser />}></Route>
      <Route path="/products" element={<Products />}></Route>
      <Route path="/basket" element={<Basket />}></Route>
    </Routes>
  </Router>,

  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
