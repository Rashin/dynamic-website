import logo from "./logo.svg";
import React from "react";
import "./App.css";
import Menu from "./Components/MainPage/Menu";
import Content from './Components/Admin/Content'

class App extends React.Component {
  constructor(props) {
    super(props);
    
  }
 
  render() {
    return (
      <div className="App">
        <div id="map">
          <Menu /> 
          <Content />
        </div>
      </div>
    );
  }
}

export default App;
