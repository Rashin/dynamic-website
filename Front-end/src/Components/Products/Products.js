import React from "react";
import "../Products/Products.css";
import Menu from "../MainPage/Menu";
import Content from "../Admin/Content";

class Products extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Menu />
        <Content>
          <span>Products</span>
        </Content>
      </div>
    );
  }
}

export default Products;
