import React from "react";
import "../Basket/Basket.css";
import Menu from "../MainPage/Menu";
import Content from "../Admin/Content";

class Basket extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Menu />
        <Content>
        <span>Basket</span>
        </Content>
      </div>
    );
  }
}

export default Basket