import React from 'react'
import '../Admin/Content.css'

class Content extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className='maincontent'>
                {this.props.children}
            </div>
        )
    }
}

export default Content