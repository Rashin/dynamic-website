import React from "react";
import "../Admin/AddUser.css";
import Menu from '../MainPage/Menu'
import Content from "./Content";

class AddUser extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Menu />
        <Content>
            Add User
        </Content>
      </div>
    );
  }
}

export default AddUser;
