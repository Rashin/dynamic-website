import React from "react";
import Menu from "../MainPage/Menu";
import Content from "../Admin/Content";

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = { apiUsersData: null };
    this.callUsersData = this.callUsersData.bind(this);
  }

  callUsersData() {
    fetch("http://localhost:4000/user/getusers")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          apiUsersData: json,
        });
      });
  }

  componentDidMount() {
    this.callUsersData();
  }

  render() {
    return (
      <div>
        <Menu />
        <Content>
          <div>
            {this.state.apiUsersData !== null ? (
              this.state.apiUsersData.map((item) => {
                return (
                  <div id={item._id}>
                    <span>{item.username}</span>
                    <p>{item.email}</p>
                  </div>
                );
              })
            ) : (
              <span>User Data</span>
            )}
          </div>
        </Content>
      </div>
    );
  }
}

export default User;
