import React from "react";

import "bootstrap/dist/css/bootstrap.css";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import "../MainPage/Menu.css";
import NavDropdown from "react-bootstrap/NavDropdown";

function Menu() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="/">Your website</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="/">Home</Nav.Link>
              <Nav.Link href="/products">Products</Nav.Link>
              <Nav.Link href="/basket">Basket</Nav.Link> 
              <NavDropdown href="/users" title="Users" id="basic-nav-dropdown">
              <NavDropdown.Item href="/users">All users</NavDropdown.Item>
              <NavDropdown.Item href="/users/add">Add user</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
}

export default Menu;
