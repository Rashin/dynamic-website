const { generateText } = require('./util')

test('should output name and age',() => {
 const text = generateText('rashin',35);
 expect(text).toBe('rashin (age 35)')
});